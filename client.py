from netmanager.NetManager import NetManager
import time
import json
import os
import sys
import traceback
import logging 
import csv

CONNECT_INTERVAL = 10 #seconds

#set logger
logger = logging.getLogger("client")

logger.setLevel(logging.DEBUG)
fileFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler = logging.FileHandler("client.log")
fileHandler.setFormatter(fileFormatter)
logger.addHandler(fileHandler)

consoleFormatter = logging.Formatter('[%(levelname)s] %(message)s')
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(consoleFormatter)
logger.addHandler(consoleHandler) 

BUFFER_SIZE = 100

isConnected = False
while True:
	try:
		netManager = NetManager(messageWithLen = True)
		ip = sys.argv[1]
		port = int(sys.argv[2])
		netManager.connect((ip, int(port)))
		isConnected = True
		logger.info("connected to %s, %d" % (ip, port))
		with open('server_log.csv', 'ab') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',')
			while (True):
				info = {}
				info["start_time"] = time.time()
				#print(info)
				netManager.send(json.dumps(info))
				received_json = json.loads(netManager.receive(BUFFER_SIZE).decode("utf-8"))
				
				start_time = received_json['start_time']
				end_time = time.time()
				diff_time = end_time - start_time
				server_time = received_json['server_time']
				
				#print ("start_time %s, end_time %s, server_time %s" % (start_time, end_time, server_time))
				spamwriter.writerow((str(start_time), str(end_time), str(diff_time), str(server_time)))
				time.sleep(0.01)
	except Exception as e:
		logger.error(str(e))
		traceback.print_exc()
	except:
		traceback.print_exc()
		logger.error(str(e))
	
	if (isConnected):
		start_time = time.time()
	isConnected = False
	end_time = time.time()
	
	if(end_time - start_time > CONNECT_INTERVAL):
		logger.info("Connect time expired")
		break
	else:
		logger.info("Try to reconnect")

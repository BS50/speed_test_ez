from netmanager.NetManager import NetManager
import sys
import socket
import json
import time
import csv
import traceback
import logging 

#set logger
logger = logging.getLogger("server")

logger.setLevel(logging.DEBUG)
fileFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler = logging.FileHandler("server.log")
fileHandler.setFormatter(fileFormatter)
logger.addHandler(fileHandler)

consoleFormatter = logging.Formatter('[%(levelname)s] %(message)s')
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(consoleFormatter)
logger.addHandler(consoleHandler) 

BUFFER_SIZE = 100

while True:
	try:
		netManager = NetManager(messageWithLen = True)
		port = int(sys.argv[1])
		logger.info(socket.gethostbyname(socket.gethostname()))
		logger.info("Start listen on port %d" % port)
		address = netManager.accept(port)
		logger.info("accepted")
		with open('log.csv', 'ab') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',')
			while(True):
				received_json = json.loads(netManager.receive(BUFFER_SIZE).decode("utf-8"))
				sent_time = received_json['start_time']
				receive_time = time.time()
				received_json['server_time'] = receive_time
				diff_time = receive_time - sent_time
				
				netManager.send(json.dumps(received_json))
			
				#print ("%f %f %f" % (sent_time, receive_time, diff_time))
				spamwriter.writerow((str(sent_time), str(receive_time), str(diff_time)))
	except Exception as e:
		logger.error(e, exc_info=True)
		traceback.print_exc()
	except:
		logger.error(str(e))
		traceback.print_exc()

